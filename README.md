# mac_grep.rb

Ruby script to derive a the manufacturer of a NIC given the MAC address

    $ mac_grep.rb 48:a5:e7:03:a5:7a 48:43:dd:ef:27:ac ac82.4724.e0eb b42e.993d.94ea 74-8F-3C-B8-DD-CC dead:beef:cafe
    48:a5:e7:03:a5:7a    Nintendo Co.,Ltd
    48:43:dd:ef:27:ac    Amazon Technologies Inc.
    ac:82:47:24:e0:eb    Intel Corporate
    b4:2e:99:3d:94:ea    GIGA-BYTE TECHNOLOGY CO.,LTD.
    74:8f:3c:b8:dd:cc    Apple, Inc.
    de:ad:be:ef:ca:fe    404 Not Found

You can give it one or several MACs in various formats (e.g. dashes, colons, etc)
